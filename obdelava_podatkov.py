import csv
import re
import numpy as np
import time as tm


ime = []
znamka = []
cena = []
seznam = [[],[],[],[],[],[],[],[],[]]


# uredim podatke
for file in ['podatki/prenosniki1.html', 'podatki/prenosniki2.html', 'podatki/prenosniki3.html', 'podatki/prenosniki4.html', 'podatki/prenosniki5.html', 'podatki/prenosniki6.html', 'podatki/prenosniki7.html', 'podatki/prenosniki8.html', 'podatki/prenosniki9.html', 'podatki/prenosniki10.html', 'podatki/prenosniki11.html', 'podatki/prenosniki12.html']: # tu dodaj

    # preberem podatke
    with open(file) as datoteka:
        vsebina = datoteka.read()


    # ime in znamka
    vzorec = '"lay-block ng-binding">\n\t\t\t\t\t(.*?)\n.*?</a>'

    for ujemanje in re.finditer(vzorec, vsebina):
        a = ujemanje.group(1)
        a = a.replace(',', ' ')
        ime.append(a)
        a = a.split(' ')
        znamka.append(a[0])


    # cena
    vzorec = '"lst-product-item-price ng-binding">(.*?)</b>'

    for ujemanje in re.finditer(vzorec, vsebina):
        a = ujemanje.group(1)
        a = a.replace('€', '')
        a = a.replace(' ', '')
        a = a.replace('.', '')
        a = a.replace(',', '.')
        a = float(a)
        cena.append(a)


    # ostali podatki
    sez = ['Velikost zaslona', 'Operacijski sistem', 'Kapaciteta trdega diska', 'Spomin \(RAM\)',     'Procesor', 'Lo.*ljivost zaslona', 'Vrsta spominskega medija', 'Frekvenca', '.*tevilo jeder procesorja'] 

    

    vzorec = ''
    stevilo = -1
    for s in sez:
        stevilo += 1
        
        vzorec = s + '</td>'
        for i in range(12):
            vzorec += '<td>.*?</td>'
        vzorec = vzorec.replace('.*?','(.*?)')
        
        podatki = seznam[stevilo]
        for i in range(1,13):
            for ujemanje in re.finditer(vzorec, vsebina):
                a = ujemanje.group(i)
                
                if ' Palcev' or ' GB' or ' MHz' in a:
                    a = a.replace(' Palcev','')
                    a = a.replace(' GB','')
                    a = a.replace(' MHz','')
                    
                if '---' in a:
                    if s == 'Vrsta spominskega medija':
                        a = a.replace('---','ni podatka')
                    else:
                        a = a.replace('---','np.nan')                        

                if ' x ' in a:
                    vz = '(\d+) x (\d+)'
                    for k in re.finditer(vz, a):
                        st1 = int(k.group(1))
                        st2 = int(k.group(2))
                        a = st1*st2
                        
                podatki += [a]
        seznam[stevilo] = podatki

print(seznam)
        
'''
prvi_stolpec = []
for i in seznam[7]:
    if i == 'np.nan':
        prvi_stolpec.append(np.nan)
    else:
        prvi_stolpec.append(int(i))

drugi_stolpec = []
for i in seznam[8]:
    if i == 'np.nan':
        drugi_stolpec.append(np.nan)
    else:
        drugi_stolpec.append(int(i))

stolpec = [a*b for a,b in zip(prvi_stolpec,drugi_stolpec)]

stolpec_int = []
for i in stolpec:
    if i is np.nan:
        stolpec_int.append(np.nan)
    else:
        stolpec_int.append(np.nan)
        
seznam[7] = stolpec
seznam = seznam[:8]
'''


# kljuc
kljuc = range(1,120)
kljuc = list(kljuc)


# dodam naslove
kljuc.insert(0,'kljuc')
ime.insert(0,'Ime')
znamka.insert(0,'Znamka')
cena.insert(0,'Cena')
seznam[0].insert(0,'Velikost zaslona')
seznam[1].insert(0,'Operacijski sistem')
seznam[2].insert(0,'Kapaciteta trdega diska')
seznam[3].insert(0,'Spomin (RAM)')
seznam[4].insert(0,'Procesor')
seznam[5].insert(0,'Locljivost zaslona')
seznam[6].insert(0,'Vrsta spominskega medija')
seznam[7].insert(0,'Frekvenca')
seznam[8].insert(0,'Stevilo jeder procesorja')
# tu dodaj


# uredim
seznam.insert(0,cena)
seznam.insert(0,znamka)
seznam.insert(0,ime)
seznam.insert(0,kljuc)
seznam = list(map(list, zip(*seznam)))

i = 0
while i < len(seznam):
    if np.nan in seznam[i]:
        del seznam[i]
        continue
        
    elif 'np.nan' in seznam[i]:
        del seznam[i]
        continue
    i += 1
    
    


# zapisem v csv datoteko
with open('urejeni_podatki.csv', 'w') as datoteka:
    writer = csv.writer(datoteka)
    writer.writerows(seznam)
    
    
                    

        
