PRENOSNIKI
=======================

Analizirala bom prenosnike in njihove glavne lastnosti. (https://www.mimovrste.com/prenosniki)

Za vsak prenosnik bom poiskala naslednje podatke:

 - znamka
 - cena
 - velikost zaslona
 - operacijski sistem
 - kapaciteta trdega diska
 - spomin(RAM)
 - procesor
 - ločljivost zaslona
 - vrsta spominskega medija
 - frekvenca
 - število jeder procesorja


Hipoteze:

 - Boljši kot je procesor, dražji je prenosnik. Kateri procesor je boljši?
 - Večji kot je RAM dražji je prenosnik.
 - velikost zaslona vpliva na ceno prenosnika.